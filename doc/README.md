## 数据库要求

数据库MySql5.7+

数据库配置的 **sql_mode** 中必须去掉 **only_full_group** 选项

初始化Sql脚本，脚本编码环境UTF-8

[financial.sql](financial.sql)
